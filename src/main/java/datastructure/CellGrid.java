package datastructure;

import cellular.CellState;

import java.util.ArrayList;

public class CellGrid implements IGrid {

    int rows;
    int cols;
    ArrayList<CellState> list;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.cols = columns;
        this.list = new ArrayList<CellState>();

        int numEntries = rows*columns;
        for(int i=0; i<numEntries; i++) { //number of cells
            list.add(initialState);
        }

    }

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
    int index = getIndex(row,column);
    list.set(index,element);
    }
    private int getIndex(int row, int cols){
        if(row < 0 || cols < 0){
            throw new IndexOutOfBoundsException("Index is to small");
        }
        else if (row > numRows() || cols > numColumns()){
            throw new IndexOutOfBoundsException("Index is to big");
        }
        return row * this.cols + cols; //number of rows and number in columns
    }

    @Override
    public CellState get(int row, int column) {
        int i = getIndex(row,column);
        return list.get(i);
    }

    @Override
    public IGrid copy() {
        CellGrid newGrid = new CellGrid(this.rows, this.cols, CellState.DEAD);
        for(int row = 0; row < this.rows; row++) {
            for(int col = 0; col < this.cols; col++) {
                CellState value = this.get(row, col);
                newGrid.set(row, col, value);
            }
        }
        return newGrid;
    }

    
}
