package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;

public class BriansBrain implements CellAutomaton {

	IGrid brainMap;

	public BriansBrain(int rows, int columns) {
	brainMap = new CellGrid(rows,columns,CellState.DEAD);
	initializeCells();
	}

	@Override
	public CellState getCellState(int row, int column) {
		return getGrid().get(row,column);
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < brainMap.numRows(); row++) {
			for (int col = 0; col < brainMap.numColumns(); col++) {
				if (random.nextBoolean()) {
					brainMap.set(row, col, CellState.ALIVE);
				} else {
					brainMap.set(row, col, CellState.DEAD);
				}
			}
		}
	}


	@Override
	public void step() {
	IGrid nextBrain = brainMap.copy();
		brainMap = nextBrain;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		boolean isAlive = getCellState(row, col) == CellState.ALIVE;
		boolean isDead = getCellState(row, col) == CellState.DEAD;
		boolean idDying = getCellState(row,col)== CellState.DYING;


		return getCellState(row, col);

	}

	@Override
	public int numberOfRows() {
		return getGrid().numRows();
	}

	@Override
	public int numberOfColumns() {
		return getGrid().numColumns();
	}

	@Override
	public IGrid getGrid() {
		return brainMap;
	}
}
